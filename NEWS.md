# drealthemes 0.0.1.9000

* Add specific discrete scale for exploration of data with `palette = "explore"` (#1)
* Modify default fonts to Lato instead of Raleway (#7)
* Added a `NEWS.md` file to track changes to the package

# drealthemes 0.0.1

* ggplot2 themes
* color themes and palettes
* First release
